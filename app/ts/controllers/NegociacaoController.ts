import { NegociacoesView, MensagemView } from '../views/index';
import { Negociacoes } from '../models/Negociacoes';
import { Negociacao } from '../models/Negociacao';
import { logarTempoDeExecucao, domInject, throttle } from "../helpers/decorators/index";
import { INegociocao } from "../models/index";
import { NegociacaoService } from "../services/index";
import { imprime } from "../helpers/index";


export class NegociacaoController
{
    @domInject("#data")
    private _inputData: JQuery;

    @domInject("#qtd")
    private _inputQtd: JQuery;

    @domInject("#valor")
    private _inputValor: JQuery;

    private _negociacoes: Negociacoes = new Negociacoes();
    //private _negociacoes: = new Negociacoes();
    private _negociacoesView = new NegociacoesView('#negociacoesView');
    private _mensagemView = new MensagemView('#mensagemView');

    private _negociacaoService = new NegociacaoService();
    

    constructor()
    {
        // this._inputData = $("#data");
        // this._inputQtd = $("#qtd");
        // this._inputValor = $("#valor");
        this._negociacoesView.update(this._negociacoes);
    }

    @throttle()
    //@logarTempoDeExecucao()
    adiciona(/*evento: Event*/)
    {
        // evento.preventDefault();

        let data = new Date(this._inputData.val().replace(/-/g,","));

        if(this._ehDiaUtil(data))
        {
            this._mensagemView.update("Somente é permitido negociações em dias úteis");
            return;
        }

        const novaNegociacao = new Negociacao
        (
            data,
            parseInt(this._inputQtd.val()),
            parseFloat(this._inputValor.val())
        );
        this._negociacoes.adiciona(novaNegociacao);
        this._negociacoesView.update(this._negociacoes);

        imprime(novaNegociacao, this._negociacoes);

        this._mensagemView.update("Negociação adicionada com sucesso!");

        // novaNegociacao.paraTexto();
        // this._negociacoes.paraTexto();


        
        /*this._negociacoes.paraArray().forEach(n => {
            console.log(n.data);
            console.log(n.qtd);
            console.log(n.valor);
        }); */
    }
    
    private _ehDiaUtil(data : Date) : boolean
    {
        return (data.getDay() != DiasDaSemana.Domingo && data.getDay() != DiasDaSemana.Sabado) ? true : false; 
    }

    @throttle()
    @logarTempoDeExecucao()
    importaDados()
    {

        function isOk(res : Response)
        {
            if(res.ok)
            {
                return res;
            }
            else
            {
                throw new Error(res.statusText);
            }
        }

        this._negociacaoService
            .obterNegociacao(isOk)
            .then(negociacao => 
            {
                negociacao.forEach(n => this._negociacoes.adiciona(n));
                
                this._negociacoesView.update(this._negociacoes);
            })
    }
}

enum DiasDaSemana
{
    Domingo,
    Segunda,
    Terça,
    Quarta,
    Quinta,
    Sexta,
    Sabado
}