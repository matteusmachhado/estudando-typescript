import { logarTempoDeExecucao } from "../helpers/decorators/index";

export abstract class View<T>
{
    protected _elemente: JQuery;
    private _escapador: boolean;

    constructor(seletor: string, escapador: boolean = false) {
        this._elemente = $(seletor);
        this._escapador = escapador;
    }

    @logarTempoDeExecucao()
    update(model: T): void {

        //const t1 = performance.now();

        let template = this.template(model);

        if(this._escapador)
            template = template.replace(/<script>[\s\S]*?<\/script>/, '');
        
        this._elemente.html(template);

        //const t2 = performance.now();

        //console.log(`Tempo de execução metodo UPDATE: ${ t2 - t1 }`);
    }

    abstract template(model: T): string;
}