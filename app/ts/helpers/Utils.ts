import { Imprimivel } from "../models/index";

export function imprime(...object : Imprimivel[])
{
    object.forEach(n => n.paraTexto());
}