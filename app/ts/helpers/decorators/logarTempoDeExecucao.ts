export function logarTempoDeExecucao(unidadeTempoSegundos: boolean = true)
{
    return function(target: any, propertyKey: string, descriptor: PropertyDescriptor)
    {
        const metodoOriginal = descriptor.value;

        descriptor.value = function(...args: any[])
        {
            let unidadeTempo = 'Milisegundos';
            let divisor = 1;
            if(unidadeTempoSegundos)
            {
                unidadeTempo = "Segundos";
                divisor = 1000;
            }

            console.log(`>_____________________DETALHES_EXE_METODO:${ propertyKey.toLocaleUpperCase() } `);
            console.log(`>__PARAMETROS: ${ args.length > 0 ? JSON.stringify(args) : "Sem Parametros" } `);
            const t1 = performance.now();
            
            const retorno = metodoOriginal.apply(this, args);
            
            const t2 = performance.now();
            console.log(`>__RETORNO: ${ (retorno && Array.isArray(retorno) && retorno.length > 0) || (retorno && !Array.isArray(retorno)) ? JSON.stringify(retorno) : "Sem retorno" } `);
            console.log(`>__TEMPO_DE_EXECUCAO: ${ (t2 - t1 ) / divisor } em ${ unidadeTempo } ` );
            console.log(">__FIM_DO_METODO...");
            return retorno;
        }
        return descriptor;
    }
}