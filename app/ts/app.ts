import { NegociacaoController } from './controllers/NegociacaoController';

const negociacao = new NegociacaoController();

$(".form").submit(negociacao.adiciona.bind(negociacao));
$("#btn-importar-dados").click(negociacao.importaDados.bind(negociacao));

/*
    document
        .querySelector('.form')
        .addEventListener('submit', negociacao.adiciona.bind(negociacao));
*/