import { Negociacao, INegociocao } from "../models/index";

export class NegociacaoService
{
    obterNegociacao(handler: HandlerFunction) : Promise<Negociacao[]>
    {
        return fetch("http://localhost:8080/dados")
        .then(res => handler(res))
        .then(result => result.json())
        .then((dados : INegociocao[]) => 
            dados
                .map(dado => new Negociacao(new Date(), dado.vezes, dado.montante ))
        );
        //.catch(erro => console.log(erro));
    }
}

export interface HandlerFunction
{
    (res : Response) : Response;
}