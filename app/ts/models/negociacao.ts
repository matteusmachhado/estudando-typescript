import { Imprimivel } from "./Imprimivel";
import { Igualavel } from "./Igualavel";

export class Negociacao implements Imprimivel, Igualavel<Negociacao>
{
    constructor(private _data:Date, private _qtd: number, private _valor: number)
    {
        
    }

    get data()
    {
        return this._data;
    }

    get qtd()
    {
        return this._qtd;
    }

    get valor()
    {
        return this._valor;
    }

    get volume()
    {
        return this._qtd * this._valor;
    }

    paraTexto() : void
    {
        console.log(
            `Data: ${this.data}
            Quantidade: ${this.qtd}, 
            Valor: ${this.valor}, 
            Volume: ${this.volume}`
        )
    }

    ehIgual(negociacao: Negociacao) : boolean
    {
        return this.data.getDate() == negociacao.data.getDate()
            && this.data.getMonth() == negociacao.data.getMonth()
            && this.data.getFullYear() == negociacao.data.getFullYear();
    }
    
}