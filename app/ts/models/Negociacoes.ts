import { Negociacao } from './Negociacao';
import { Imprimivel } from './Imprimivel';
import { Igualavel } from "./Igualavel";
import * as _ from 'lodash';

export class Negociacoes implements Imprimivel, Igualavel<Negociacoes>
{
    private _negociacoes: Array<Negociacao> = [];
    // priate _negociacoes: Negociacao[];

    adiciona(negociacao: Negociacao) : void
    {
        this._negociacoes.push(negociacao);
    }

    paraArray() : Negociacao[]
    {
        return _.clone(this._negociacoes);
    }

    paraTexto() : void
    {
        console.log(JSON.stringify(this._negociacoes));
    }
    ehIgual(negociacoes: Negociacoes) : boolean
    {
        return JSON.stringify(this._negociacoes) == JSON.stringify(negociacoes.paraArray);
    }

}